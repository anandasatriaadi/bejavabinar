package dev.annd;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

    /* ========================================================================
        START ::: CLASS SCOPED VARIABLES
    ======================================================================== */
    private static final Scanner inputScanner = new Scanner(System.in);

    private static final HashMap<String, String[]> menus = new HashMap<String, String[]>() {
        {
            put(
                "main_menu",
                new String[] {
                    "",
                    "//line//",
                    "Kalkulator Penghitung Luas dan Volume",
                    "//line//",
                    "Menu",
                    "1. Hitung Luas Bidang",
                    "2. Hitung Volume",
                    "0. Tutup Aplikasi",
                    "//line//",
                }
            );
            put(
                "area_menu",
                new String[] {
                    "",
                    "//line//",
                    "Pilih Luas Bidang yang Akan Dihitung",
                    "//line//",
                    "1. Persegi",
                    "2. Persegi Panjang",
                    "3. Lingkaran",
                    "4. Segitiga",
                    "0. Kembali ke Menu Sebelumnya",
                    "//line//",
                }
            );
            put(
                "volume_menu",
                new String[] {
                    "//line//",
                    "Pilih Volume Bangun yang Akan Dihitung",
                    "//line//",
                    "1. Kubus",
                    "2. Balok",
                    "3. Tabung",
                    "0. Kembali ke Menu Sebelumnya",
                    "//line//",
                }
            );
        }
    };

    /* ========================================================================
        END ::: CLASS SCOPED VARIABLES
    ======================================================================== */

    /* ========================================================================
        START ::: HELPER FUNCTIONS
    ======================================================================== */
    private static int longestString(String[] strArr) {
        int longest = 0;
        for (String s : strArr) {
            if (s.length() > longest) {
                longest = s.length();
            }
        }

        return longest;
    }

    private static void printMenu(String[] strArr) {
        int maxChar = longestString(strArr) + 6;
        String line = "";
        for (int i = 0; i < maxChar; i++) {
            line = line.concat("-");
        }
        for (String s : strArr) {
            if (s.equals("//line//")) {
                System.out.println(line);
            } else {
                System.out.println("   " + s);
            }
        }
    }

    private static int scanInt(String inputMsg) {
        System.out.print(inputMsg);
        return inputScanner.nextInt();
    }

    private static double scanDouble(String inputMsg) {
        System.out.print(inputMsg);
        return inputScanner.nextDouble();
    }

    /* ========================================================================
        END ::: HELPER FUNCTIONS
    ======================================================================== */

    /* ========================================================================
        START ::: CALCULATE AREA FUNCTIONS
    ======================================================================== */
    public static void areaMenu() {
        int shapeType = 0;

        printMenu(menus.get("area_menu"));
        shapeType = (int) scanInt(" > ");

        switch (shapeType) {
            /* ========================================================================
                Persegi
            ======================================================================== */
            case 1:
                printMenu(new String[] { "", "//line//", "Anda memilih Persegi", "//line//" });

                /* ======== Get input from user ======== */
                double sisi_persegi = scanDouble("Panjang Sisi : ");
                System.out.println("\nCalculating...");

                /* ======== Calculate area and print to user ======== */
                printMenu(
                    new String[] {
                        "//line//",
                        "Luas dari persegi adalah " + calculateArea(shapeType, new double[] { sisi_persegi }),
                        "//line//",
                    }
                );

                /* ======== Back to main menu after calculation printed. ======== */
                System.out.println("Tekan ENTER untuk kembali ke menu utama.");
                inputScanner.nextLine();
                inputScanner.nextLine();
                break;
            /* ========================================================================
                Persegi Panjang
            ======================================================================== */
            case 2:
                printMenu(new String[] { "", "//line//", "Anda memilih Persegi Panjang", "//line//" });

                /* ======== Get input from user ======== */
                double panjang = scanDouble("Panjang : ");
                double lebar = scanDouble("Lebar : ");
                System.out.println("\nCalculating...");

                /* ======== Calculate area and print to user ======== */
                printMenu(
                    new String[] {
                        "//line//",
                        "Luas dari persegi panjang adalah " + calculateArea(shapeType, new double[] { panjang, lebar }),
                        "//line//",
                    }
                );

                /* ======== Back to main menu after calculation printed. ======== */
                System.out.println("Tekan ENTER untuk kembali ke menu utama.");
                inputScanner.nextLine();
                inputScanner.nextLine();
                break;
            /* ========================================================================
                Lingkaran
            ======================================================================== */
            case 3:
                printMenu(new String[] { "", "//line//", "Anda memilih Lingkaran", "//line//" });

                /* ======== Get input from user ======== */
                double jari_jari = scanDouble("Jari-Jari Lingkaran : ");
                System.out.println("\nCalculating...");

                /* ======== Calculate area and print to user ======== */
                printMenu(
                    new String[] {
                        "//line//",
                        "Luas dari lingkaran adalah " + calculateArea(shapeType, new double[] { jari_jari }),
                        "//line//",
                    }
                );

                /* ======== Back to main menu after calculation printed. ======== */
                System.out.println("Tekan ENTER untuk kembali ke menu utama.");
                inputScanner.nextLine();
                inputScanner.nextLine();
                break;
            /* ========================================================================
                Segitiga
            ======================================================================== */
            case 4:
                printMenu(new String[] { "", "//line//", "Anda memilih Segitiga", "//line//" });

                /* ======== Get input from user ======== */
                double alas = scanDouble("Panjang Alas : ");
                double tinggi = scanDouble("Tinggi : ");
                System.out.println("\nCalculating...");

                /* ======== Calculate area and print to user ======== */
                printMenu(
                    new String[] {
                        "//line//",
                        "Luas dari segitiga adalah " + calculateArea(shapeType, new double[] { alas, tinggi }),
                        "//line//",
                    }
                );

                /* ======== Back to main menu after calculation printed. ======== */
                System.out.println("Tekan ENTER untuk kembali ke menu utama.");
                inputScanner.nextLine();
                inputScanner.nextLine();
                break;
            default:
                break;
        }
    }

    public static double calculateArea(int shapeType, double[] sides) {
        /* ========================================================================
            Shape Types:
            1. Persegi   2. Lingkaran   3. Segitiga   4. Persegi panjang
        ======================================================================== */
        switch (shapeType) {
            case 1:
                return sides[0] * sides[0];
            case 2:
                return sides[0] * sides[1];
            case 3:
                return 3.14 * sides[0] * sides[0];
            case 4:
                return sides[0] * sides[1] / 2;
            default:
                return 0;
        }
    }

    /* ========================================================================
        END ::: CALCULATE AREA FUNCTIONS
    ======================================================================== */

    /* ========================================================================
        START ::: CALCULATE VOLUME FUNCTIONS
    ======================================================================== */
    public static void volumeMenu() {
        int shapeType = 0;

        printMenu(menus.get("volume_menu"));
        shapeType = (int) scanInt(" > ");

        switch (shapeType) {
            /* ========================================================================
                Kubus
            ======================================================================== */
            case 1:
                printMenu(new String[] { "", "//line//", "Anda memilih Kubus", "//line//" });

                /* ======== Get input from user ======== */
                double kubus_sisi = scanDouble("Panjang Sisi : ");
                System.out.println("\nCalculating...");

                /* ======== Calculate volume and print to user ======== */
                printMenu(
                    new String[] {
                        "//line//",
                        "Volume dari kubus adalah " + calculateVolume(shapeType, new double[] { kubus_sisi }),
                        "//line//",
                    }
                );

                /* ======== Back to main menu after calculation printed. ======== */
                System.out.println("Tekan ENTER untuk kembali ke menu utama.");
                inputScanner.nextLine();
                inputScanner.nextLine();
                break;
            /* ========================================================================
                Balok
            ======================================================================== */
            case 2:
                printMenu(new String[] { "", "//line//", "Anda memilih Balok", "//line//" });

                /* ======== Get input from user ======== */
                double balok_panjang = scanDouble("Panjang Balok : ");
                double balok_lebar = scanDouble("Lebar Balok : ");
                double balok_tinggi = scanDouble("Tinggi Balok : ");
                System.out.println("\nCalculating...");

                /* ======== Calculate volume and print to user ======== */
                printMenu(
                    new String[] {
                        "//line//",
                        "Volume dari balok adalah " + calculateVolume(shapeType, new double[] { balok_panjang, balok_lebar, balok_tinggi }),
                        "//line//",
                    }
                );

                /* ======== Back to main menu after calculation printed. ======== */
                System.out.println("Tekan ENTER untuk kembali ke menu utama.");
                inputScanner.nextLine();
                inputScanner.nextLine();
                break;
            /* ========================================================================
                Tabung
            ======================================================================== */
            case 3:
                printMenu(new String[] { "", "//line//", "Anda memilih Tabung", "//line//" });

                /* ======== Get input from user ======== */
                double tabung_jari_jari = scanDouble("Jari-Jari : ");
                double tabung_tinggi = scanDouble("Tinggi : ");
                System.out.println("\nCalculating...");

                /* ======== Calculate volume and print to user ======== */
                printMenu(
                    new String[] {
                        "//line//",
                        "Volume dari tabung adalah " + calculateVolume(shapeType, new double[] { tabung_jari_jari, tabung_tinggi }),
                        "//line//",
                    }
                );

                /* ======== Back to main menu after calculation printed. ======== */
                System.out.println("Tekan ENTER untuk kembali ke menu utama.");
                inputScanner.nextLine();
                inputScanner.nextLine();
                break;
            default:
                break;
        }
    }

    public static double calculateVolume(int shapeType, double[] sides) {
        /* ========================================================================
            Shape Types:
            1 > Kubus   2 > Balok   3 > Tabung
        ======================================================================== */
        switch (shapeType) {
            case 1:
                return sides[0] * sides[0] * sides[0];
            case 2:
                return sides[0] * sides[1] * sides[2];
            case 3:
                return 3.14 * sides[0] * sides[0] * sides[1];
            default:
                return 0;
        }
    }

    /* ========================================================================
        END ::: CALCULATE VOLUME FUNCTIONS
    ======================================================================== */

    /* ========================================================================
        MAIN FUNCTION CALL
    ======================================================================== */
    public static void main(String[] args) {
        int menuType = 1;
        while (menuType != 0) {
            printMenu(menus.get("main_menu"));
            menuType = scanInt(" > ");

            if (menuType == 1) {
                areaMenu();
            } else if (menuType == 2) {
                volumeMenu();
            } else {
                continue;
            }
        }

        inputScanner.close();
    }
}
